/*
* AUTHOR: Margaux Boileau
* DATE: 2023/02/19
* TITLE: 4- Làmpada d’Alí Babar
*/

fun main() {

    val mechanicalArm = MechanicalArm()

    mechanicalArm.toggle()
    println(mechanicalArm)
    mechanicalArm.updateAltitude(3.0)
    println(mechanicalArm)
    mechanicalArm.updateAngle(180.0)
    println(mechanicalArm)
    mechanicalArm.updateAltitude(-3.0)
    println(mechanicalArm)
    mechanicalArm.updateAngle(-180.0)
    println(mechanicalArm)
    mechanicalArm.updateAltitude(3.0)
    println(mechanicalArm)
    mechanicalArm.toggle()
    println(mechanicalArm)
}

class MechanicalArm(var openAngle: Double = 0.0, var altitude: Double = 0.0, var turnedOn: Boolean = false) {
    fun updateAngle(n: Double) {
        if (turnedOn) {
            if ((0.0..360.0).contains(openAngle + n)) {
                openAngle += n
            }
            else if ((openAngle + n) > 360) openAngle = openAngle + n - 360
            else if ((openAngle + n) < 0) openAngle = (openAngle + n) * - 1
        }
    }

    fun updateAltitude(n: Double) {
        if (turnedOn) if ((0.0..30.0).contains(altitude + n)) altitude += n
    }

    fun toggle() {
        turnedOn = !turnedOn
    }
}