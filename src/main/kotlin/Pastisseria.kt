/*
* AUTHOR: Margaux Boileau
* DATE: 2023/02/19
* TITLE: 1- Pastisseria & 2- Pastisseria
*/

fun main(){
    val croissant = Pasta("Croissant", 1.2, 60, 200)
    val ensaimada = Pasta("Ensaimada", 1.4, 100,290 )
    val donut = Pasta("Donut", 1.0, 100, 270 )

    val aigua = Beguda("Aigua", 1.00 , false)
    val cafe = Beguda("Café tallat", 1.35 ,false)
    val te = Beguda("Té vermell" ,1.50 ,false)
    val cola = Beguda("Cola", 1.65 ,true)

    val pastes = listOf(croissant,ensaimada,donut)
    val begudes = listOf(aigua,cafe,te,cola)

    println("Pastes:")
    for (pasta in pastes) {
        println("${pasta.nom}: ${pasta.preu}€, ${pasta.pes}g, ${pasta.calories}cals")
    }
    println()
    println("Begudes:")
    for (beguda in begudes) {
        println("${beguda.nom}: ${beguda.preu}€")
    }
}

data class Pasta ( val nom:String, val preu:Double, val pes:Int,val calories:Int)

class Beguda ( val nom:String, var preu:Double, sucre:Boolean) {
    init{
        if (sucre) preu += preu * 0.10
    }
}