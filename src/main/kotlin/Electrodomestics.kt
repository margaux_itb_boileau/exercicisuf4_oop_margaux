/*
* AUTHOR: Margaux Boileau
* DATE: 2023/02/19
* TITLE: 6- Electrodomèstics
*/

fun main() {

    var preuBaseRentadores = 0
    var preuFinalRentadores = 0
    var preuBaseTelevisio = 0
    var preuFinalTelevisio = 0
    var preuBaseElectrodomestic = 0
    var preuFinalElectrodomestic = 0


    val electrodomestics = arrayListOf(
        Electrodomestic(35, 'D', 2),
        Electrodomestic(6, 'A', 2),
        Electrodomestic(6, 'A', 2),
        Electrodomestic(6, 'A', 2),
        Electrodomestic(6, 'A', 2),
        Electrodomestic(6, 'A', 2),
        Rentadora(5, 400),
        Rentadora(8, 500),
        Televisio(52, 125),
        Televisio(28, 700)
    )

    for (i in electrodomestics.indices) {
        val electrodomestic = electrodomestics[i]
        println(
            "Electrodomestic ${i + 1}:\n" +
                    "\tPreu base: ${electrodomestic.preuBase}€\n" +
                    "\tColor: ${electrodomestic.color}\n" +
                    "\tConsum: ${electrodomestic.consum}\n" +
                    "\tPes: ${electrodomestic.pes}kg\n" +
                    "\tPreu final: ${electrodomestic.preuFinal()}€\n"
        )

        if (electrodomestic is Rentadora) {
            preuBaseRentadores += electrodomestic.preuBase
            preuFinalRentadores += electrodomestic.preuFinal()
        } else if (electrodomestic is Televisio) {
            preuBaseTelevisio += electrodomestic.preuBase
            preuFinalTelevisio += electrodomestic.preuFinal()
        }

        preuBaseElectrodomestic += electrodomestic.preuBase
        preuFinalElectrodomestic += electrodomestic.preuFinal()
    }

    println("Electrodomèstics:\n" +
            "\tPreu base: $preuBaseElectrodomestic€\n" +
            "\tPreu final: $preuFinalElectrodomestic€\n" +
            "Rentadores:\n" +
            "\tPreu base: $preuBaseRentadores€\n" +
            "\tPreu final: $preuFinalRentadores€\n" +
            "Televisions:\n" +
            "\tPreu base: $preuBaseTelevisio€\n" +
            "\tPreu final: $preuFinalTelevisio€\n"
    )

}

open class Electrodomestic(
    var preuBase: Int,
    open var consum: Char = 'B',
    var pes: Int = 5,
    var color: String = "Blanc"
) {

    open fun preuFinal(): Int {
        var preuFinal = preuBase
        when (consum) {
            'A' -> preuFinal += 35
            'B' -> preuFinal += 30
            'C' -> preuFinal += 25
            'D' -> preuFinal += 20
            'E' -> preuFinal += 15
            'F' -> preuFinal += 10
        }

        if ((6..20).contains(pes)) preuFinal += 20
        else if ((20..50).contains(pes)) preuFinal += 50
        else if ((50..80).contains(pes)) preuFinal += 80
        else if (pes > 80) preuFinal += 100

        return preuFinal
    }
}

class Rentadora(val carrega: Int = 5, preuBase: Int) : Electrodomestic(preuBase) {
    override fun preuFinal(): Int {

        var preuFinal = preuBase

        if ((6..7).contains(carrega)) preuFinal += 55
        else if (carrega == 8) preuFinal += 70
        else if (carrega == 9) preuFinal += 85
        else if (carrega == 10) preuFinal += 100

        return preuFinal
    }

}

class Televisio(val tamany: Int = 28, preuBase: Int) : Electrodomestic(preuBase) {
    override fun preuFinal(): Int {
        var preuFinal = preuBase

        if ((29..32).contains(tamany)) preuFinal += 55
        else if ((32..42).contains(tamany)) preuFinal += 70
        else if ((42..50).contains(tamany)) preuFinal += 85
        else if (tamany > 50) preuFinal += 100

        return preuFinal
    }

}