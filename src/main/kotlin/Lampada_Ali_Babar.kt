/*
* AUTHOR: Margaux Boileau
* DATE: 2023/02/19
* TITLE: 1.12 De Celsius a Fahrenheit
*/

const val ANSI_RESET = "\u001B[0m"
const val ANSI_BLACK = "\u001B[40m"
const val ANSI_RED = "\u001B[41m"
const val ANSI_GREEN = "\u001B[42m"
const val ANSI_YELLOW = "\u001B[43m"
const val ANSI_BLUE = "\u001B[44m"
const val ANSI_PURPLE = "\u001B[45m"
const val ANSI_CYAN = "\u001B[46m"
const val ANSI_WIHTE = "\u001B[47m"

fun main() {
    val lampades = mutableListOf(Lampada("Cuina"), Lampada("Menjador"))
    val ordresLamp1 = listOf(
        "TURN ON",
        "CHANGE COLOR",
        "CHANGE COLOR",
        "CHANGE COLOR",
        "INTENSITY",
        "INTENSITY",
        "INTENSITY",
        "INTENSITY"
    )
    val ordresLamp2 = listOf(
        "TURN ON",
        "CHANGE COLOR",
        "CHANGE COLOR",
        "INTENSITY",
        "INTENSITY",
        "INTENSITY",
        "INTENSITY",
        "TURN OFF",
        "CHANGE COLOR",
        "TURN ON",
        "CHANGE COLOR",
        "INTENSITY",
        "INTENSITY",
        "INTENSITY",
        "INTENSITY",
        "INTENSITY"
    )
    val ordres = mutableListOf(ordresLamp1, ordresLamp2)

    for (i in lampades.indices) {
        val lamp = lampades[i]
        println("Làmpada: ${lamp.id}")
        for (instr in ordres[i]) {
            when (instr) {
                "TURN ON" -> lamp.encendre()
                "TURN OFF" -> lamp.apagar()
                "CHANGE COLOR" -> lamp.canviaColor()
                "INTENSITY" -> lamp.canviaIntensitat()
            }
            println("Color: ${lamp.colors[lamp.color]}    $ANSI_RESET - intensitat ${lamp.intensitat}")
        }
        println()
    }
}

class Lampada(val id: String, var color: Int = 1, var intensitat: Int = 1) {
    val colors = arrayOf(ANSI_BLACK, ANSI_WIHTE, ANSI_RED, ANSI_GREEN, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN)
    var offColor = 0

    fun encendre() {
        if (color == 0) {
            color = offColor
            intensitat = 1
        }
    }

    fun apagar() {
        offColor = color
        color = 0
        intensitat = 0
    }

    fun canviaColor() {
        if (color in 1..colors.lastIndex) color++
        else color = 0
    }

    fun canviaIntensitat() {
        if (intensitat < 5) intensitat++
        else intensitat = 1
    }
}