/*
* AUTHOR: Margaux Boileau
* DATE: 2023/02/19
* TITLE: 3- Carpinteria
*/

fun main() {
    val llistaTaulells = listOf(
        Taulell(15 , 20, 12),
        Taulell(25, 20, 20),
        Taulell (15, 2, 5),
        Taulell(10, 20, 12))

    val llistaLlistons = listOf(Llisto(3, 4), Llisto(2, 6))

    val total = Tiquet(llistaTaulells,llistaLlistons).preuTotal

    println("El preu total és: $total €")

}

class Taulell(preuUnitari: Int, llargada:Int, amplada:Int) {
    val preuTaulell = llargada * amplada * preuUnitari
}

class Llisto (preuUnitari:Int, llargada:Int) {
    val preuLlisto = llargada * preuUnitari
}

class Tiquet (taulellsComprats: List<Taulell>?=null, llistonsComprats: List<Llisto>?=null) {
    var preuTotal:Int=0
    init {
        if (taulellsComprats != null) {
            for (taulell in taulellsComprats) preuTotal += taulell.preuTaulell
        }
        if (llistonsComprats != null) {
            for (llisto in llistonsComprats) preuTotal += llisto.preuLlisto
        }
    }
}